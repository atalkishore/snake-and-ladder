package play;

import junit.framework.TestCase;
import org.junit.Test;
import play.model.Board;
import play.service.CrookedDice;
import play.service.Dice;
import play.service.NormalDice;
import play.service.SnakeAndLadderService;

import java.util.Random;

public class GameTest {

    @Test
    public void shouldPlayGameWithNormalDice() {

        Board board = new Board();
        Dice dice = new NormalDice(new Random());
        SnakeAndLadderService snakeAndLadderService = new SnakeAndLadderService(board, dice);
        Game game = new Game(snakeAndLadderService);

        game.start(50);

    }

    @Test
    public void shouldPlayGameWithCrookedDice() {

        Board board = new Board();
        Dice dice = new CrookedDice(new Random());
        SnakeAndLadderService snakeAndLadderService = new SnakeAndLadderService(board, dice);
        Game game = new Game(snakeAndLadderService);

        game.start(50);

    }
}
