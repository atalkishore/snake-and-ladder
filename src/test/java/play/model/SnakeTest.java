package play.model;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

public class SnakeTest {

    @Test
    public void shouldAddSnake() {
        Snake snake = new Snake(14, 7);
        Assert.assertEquals(snake.getStart(), 14);
        Assert.assertEquals(snake.getEnd(), 7);
    }
}
