package play.model;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;

public class BoardTest {

    @Test
    public void shouldCreateEmptyBoardWithInitialPositionZero() {
        Board board = new Board();
        int playerPosition = board.getPlayerPosition();
        HashMap<Integer, Snake> snakes = board.getSnakes();
        Assert.assertEquals(playerPosition, 0);
        Assert.assertEquals(snakes.size(), 0);
    }

    @Test
    public void shouldCreateBoardWithSnakes() {
        Board board = new Board();
        int playerPosition = board.getPlayerPosition();
        Snake snake1 = new Snake(10, 2);
        Snake snake2 = new Snake(2, 1);
        board.setSnakes(Arrays.asList(snake1, snake2));
        HashMap<Integer, Snake> snakes = board.getSnakes();
        Assert.assertEquals(playerPosition, 0);
        Assert.assertEquals(snakes.size(), 2);
        Assert.assertEquals(snakes.get(10), snake1);
        Assert.assertEquals(snakes.get(2), snake2);
    }

    @Test
    public void shouldNotBeAbleToAddSnakesWithSameStartPosition() {
        Board board = new Board();
        int playerPosition = board.getPlayerPosition();
        Snake snake1 = new Snake(10, 2);
        Snake snake2 = new Snake(10, 7);
        board.setSnakes(Arrays.asList(snake1, snake2));
        HashMap<Integer, Snake> snakes = board.getSnakes();
        Assert.assertEquals(playerPosition, 0);
        Assert.assertEquals(snakes.size(), 1);
        Assert.assertEquals(snakes.get(10), snake2);
    }
}
