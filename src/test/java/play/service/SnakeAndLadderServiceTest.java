package play.service;

import org.junit.Test;
import org.mockito.Mockito;
import play.model.Board;
import play.model.Snake;

import java.util.Arrays;
import java.util.List;

public class SnakeAndLadderServiceTest {

    @Test
    public void shoudSetSnakes() {
        Board board = Mockito.mock(Board.class);
        Dice dice = Mockito.mock(NormalDice.class);
        SnakeAndLadderService snakeAndLadderService = new SnakeAndLadderService(board, dice);
        Snake snake1 = new Snake(10, 2);
        Snake snake2 = new Snake(23, 7);
        List<Snake> snakes = Arrays.asList(snake1, snake2);
        snakeAndLadderService.setSnakes(snakes);

        Mockito.verify(board).setSnakes(snakes);

    }

    @Test
    public void shoudMovePlayerToNewPosition() {
        Board board = Mockito.mock(Board.class);
        Dice dice = Mockito.mock(NormalDice.class);
        SnakeAndLadderService snakeAndLadderService = new SnakeAndLadderService(board, dice);


        Mockito.when(dice.roll()).thenReturn(3);
        Mockito.when(board.getPlayerPosition()).thenReturn(0);
        Mockito.when(board.getSize()).thenReturn(100);

        snakeAndLadderService.rollAndMove();

        Mockito.verify(board).setPlayerPosition(3);

    }

    @Test
    public void shoudMovePlayerToNewPositionAfterSnakesAreEncountered() {
        Board board = Mockito.mock(Board.class);
        Dice dice = Mockito.mock(NormalDice.class);
        SnakeAndLadderService snakeAndLadderService = new SnakeAndLadderService(board, dice);
        Snake snake1 = new Snake(10, 2);
        Snake snake2 = new Snake(23, 10);
        List<Snake> snakes = Arrays.asList(snake1, snake2);
        snakeAndLadderService.setSnakes(snakes);

        Mockito.when(dice.roll()).thenReturn(2);
        Mockito.when(board.getPlayerPosition()).thenReturn(0);
        Mockito.when(board.getSize()).thenReturn(100);

        snakeAndLadderService.rollAndMove();

        Mockito.verify(board).setPlayerPosition(2);

    }

    @Test
    public void shouldNotExceedBoardSize() {
        Board board = Mockito.mock(Board.class);
        Dice dice = Mockito.mock(NormalDice.class);
        SnakeAndLadderService snakeAndLadderService = new SnakeAndLadderService(board, dice);

        Mockito.when(dice.roll()).thenReturn(6);
        Mockito.when(board.getPlayerPosition()).thenReturn(98);
        Mockito.when(board.getSize()).thenReturn(100);

        snakeAndLadderService.rollAndMove();

        Mockito.verify(board).setPlayerPosition(100);

    }

}
