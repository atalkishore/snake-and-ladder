package play.service;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.util.Random;

import static org.mockito.Mockito.*;

public class CrookedDiceTest {
    @Test
    public void shouldRollOnlyEvenNumbers() {
        Random randomMock = mock(Random.class);
        Dice diceService = new CrookedDice(randomMock);

        ArgumentCaptor<Integer> bound = ArgumentCaptor.forClass(Integer.class);
        when(randomMock.nextInt(bound.capture())).thenReturn(0);

        int roll = diceService.roll();

        Assert.assertEquals(2, roll);
        Assert.assertEquals(3, bound.getValue().intValue());
    }
}
