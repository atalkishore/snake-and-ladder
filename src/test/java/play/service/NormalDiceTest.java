package play.service;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.Random;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NormalDiceTest {
    @Test
    public void shouldRollAndVerifyMaxValue() {
        Random randomMock = mock(Random.class);
        Dice diceService = new NormalDice(randomMock);

        ArgumentCaptor<Integer> bound = ArgumentCaptor.forClass(Integer.class);
        when(randomMock.nextInt(bound.capture())).thenReturn(0);

        int roll = diceService.roll();

        Assert.assertEquals(1, roll);
        Assert.assertEquals(6, bound.getValue().intValue());
    }
}
