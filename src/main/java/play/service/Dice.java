package play.service;

public interface Dice {
    int MAX_VALUE = 6;

    int roll();
}
