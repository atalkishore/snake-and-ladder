package play.service;

import play.model.Board;
import play.model.Snake;

import java.util.List;

public class SnakeAndLadderService {
    private final Board board;
    private final Dice dice;

    public SnakeAndLadderService(Board board, Dice dice) {
        this.board = board;
        this.dice = dice;
    }

    public void setSnakes(List<Snake> snakes) {
        board.setSnakes(snakes);
    }

    public void rollAndMove() {
        if (isWon()) {
            System.out.println("player has won the Game");
        } else {
            int diceValue = dice.roll();
            System.out.println("Dice " + diceValue);

            int playerNewPosition = board.getPlayerPosition() + diceValue;
            if (playerNewPosition >= board.getSize()) {
                board.setPlayerPosition(board.getSize());
            } else {
                checkForSnakeAndSetNewPosition(playerNewPosition);
            }
            System.out.println("player moved to position " + board.getPlayerPosition());
        }
    }

    private void checkForSnakeAndSetNewPosition(int playerNewPosition) {
        int prevPosition = playerNewPosition;
        Snake snake;
        do {
            snake = board.getSnake(prevPosition);

            if (snake != null) prevPosition = snake.getEnd();

        } while (snake != null);
        board.setPlayerPosition(prevPosition);
    }

    private boolean isWon() {
        int playerPosition = board.getPlayerPosition();
        return playerPosition >= board.getSize();
    }
}
