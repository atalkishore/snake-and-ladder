package play.service;

import java.util.Random;

public class CrookedDice implements Dice {
    Random random;

    public CrookedDice(Random random) {
        this.random = random;
    }


    public int roll() {

        return (random.nextInt(MAX_VALUE / 2) + 1) * 2;
    }
}
