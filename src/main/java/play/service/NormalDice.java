package play.service;

import java.util.Random;

public class NormalDice implements Dice {
    Random random;

    public NormalDice(Random random) {
        this.random = random;
    }


    public int roll() {
        return random.nextInt(MAX_VALUE) + 1;
    }
}
