package play;

import play.model.Snake;
import play.service.SnakeAndLadderService;

import java.util.Arrays;

public class Game {

    private final SnakeAndLadderService snakeAndLadderService;

    public Game(SnakeAndLadderService snakeAndLadderService) {
        this.snakeAndLadderService = snakeAndLadderService;

    }


    public void start(int times) {
        Snake snake1 = new Snake(5, 2);
        Snake snake2 = new Snake(14, 7);
        Snake snake3 = new Snake(20, 10);
        Snake snake4 = new Snake(11, 5);
        snakeAndLadderService.setSnakes(Arrays.asList(snake1, snake2, snake3, snake4));
        for (int i = 0; i < times; i++) {
            snakeAndLadderService.rollAndMove();
        }

    }
}
