package play.model;

import java.util.HashMap;
import java.util.List;

public class Board {
    private final int size;
    private int playerPosition;
    private final HashMap<Integer, Snake> snakesMap;

    public Board() {
        this.size = 100;
        playerPosition = 0;
        this.snakesMap = new HashMap<>();
    }

    public Snake getSnake(int position) {
        return snakesMap.get(position);
    }

    public HashMap<Integer, Snake> getSnakes() {
        return snakesMap;
    }

    public void setSnakes(List<Snake> snakes) {
        snakes.forEach(snake -> snakesMap.put(snake.getStart(), snake));
    }

    public int getPlayerPosition() {
        return playerPosition;
    }

    public void setPlayerPosition(int playerPosition) {
        this.playerPosition = playerPosition;
    }

    public int getSize() {
        return size;
    }
}
