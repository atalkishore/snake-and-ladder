package play.model;

public class Snake {
    private int start;
    private int end;

    public Snake(int start, int end) {
//        if (start > 100 || end < 0 || start <= end) throw new Exception("Invalid snake Positions");
        this.start = start;
        this.end = end;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }
}
